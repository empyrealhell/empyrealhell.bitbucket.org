function packVector(vector) {
  return string;
}

function unpackVector(packedString) {
}

function resetTransform(context) {
  context.setTransform(1, 0, 0, 1, 0, 0);
}

function renderVector(context, vector) {
  context.setTransform(vector.scale.x, 0, 0, vector.scale.y, vector.center.x, vector.center.y);
  context.rotate(vector.angle);
  context.beginPath();
  context.moveTo(vector.nodes[0].x, vector.nodes[0].y);
  for (var i = 1; i < vector.nodes.length; i++) {
    var node = vector.nodes[i];
    if (node.isCurve)
      context.bezierCurveTo(node.anchor[0].x, node.anchor[0].y, node.anchor[1].x, node.anchor[1].y, node.x, node.y);
    else
      context.lineTo(node.x, node.y);
  }
  if (vector.hasShadow) {
    context.shadowBlur = vector.shadowSize;
    context.shadowColor = vector.shadowColor;
  }
  if (vector.isFilled) {
    context.fillStyle = vector.color;
    context.fill();
  } else {
    context.lineStyle = vector.lineColor;
    context.lineWidth = vector.lineWidth;
    context.stroke();
  }
}

function _renderSingleNode(context, vector, nodeIndex, size) {
  var node = vector.nodes[nodeIndex];
  context.beginPath();
  context.arc(node.x, node.y, size, 0, 2 * Math.PI);
  context.fill();
  if (node.isCurve) {
    context.moveTo(node.x, node.y);
    context.lineTo(node.anchor[0].x
  }
}

function renderVectorNodes(context, vector, size) {
  context.setTransform(vector.scale.x, 0, 0, vector.scale.y, vector.center.x, vector.center.y);
  context.rotate(vector.angle);
  for (var i = 0; i < vector.nodes.length; i++) {
    context.beginPath();
    _renderSingleNode(context, vector, i, size)
  }
  if (vector.hasShadow) {
    context.shadowBlur = vector.shadowSize;
    context.shadowColor = vector.shadowColor;
  }
  if (vector.isFilled) {
    context.fillStyle = vector.color;
    context.fill();
  } else {
    context.lineStyle = vector.lineColor;
    context.lineWidth = vector.lineWidth;
    context.stroke();
  }
}