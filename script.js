function main() {

  var canvas = document.getElementById("canvas");
  var ctx = canvas.getContext("2d");
  
  var scaleFactor = { x: 10.0, y: 10.0 };
  
  var ship = {
    x: 320,
    y: 180,
    model: {
      width: 32,
      height: 32,
	  mirrorY: true,
      points: [{ x: 0, y: 16}, { x: 0, y: 0}, { x: 32, y: 16 }, ],
      guns: [{ x: 32, y: 16 }]
    }
  };
  
  var isDragging = false;
  var selectedIndices = [];
  var clickPosition = { x: -1, y: -1 };
  var hoverPosition = { x: 0, y: 0 };
  
  function clampNumber(min, max, number) {
    if (number < min)
	  return min;
	if (number > max)
	  return max;
	return number;
  }
  
  function getGridCoordinates(mouseX, mouseY) {
    var canvasBounds = canvas.getBoundingClientRect();
    var gridStart = {
      x: canvasBounds.x + (canvasBounds.width - ship.model.width * scaleFactor.x) / 2.0,
      y: canvasBounds.y + (canvasBounds.height - ship.model.height * scaleFactor.y) / 2.0
    };
    return {
      x: clampNumber(0, ship.model.width, Math.round((mouseX - gridStart.x) / scaleFactor.x)),
      y: clampNumber(0, ship.model.height, Math.round((mouseY - gridStart.y) / scaleFactor.y))
    };
  }
  
  document.onmousedown = function(event) {
	var canvasBounds = canvas.getBoundingClientRect();
    if (event.clientX < canvasBounds.x || event.clientX > canvas.x + canvasBounds.width || event.clientY < canvasBounds.y || event.clientY > canvas.y + canvasBounds.height)
	  return;
    clickPosition = getGridCoordinates(event.clientX, event.clientY);
  };
  
  document.onmousemove = function(event) {
	var canvasBounds = canvas.getBoundingClientRect();
    if (event.clientX < canvasBounds.x || event.clientX > canvas.x + canvasBounds.width || event.clientY < canvasBounds.y || event.clientY > canvas.y + canvasBounds.height)
	  return;
    hoverPosition = getGridCoordinates(event.clientX, event.clientY);
    if (clickPosition.x == -1 || clickPosition.y == -1)
      return;
    if (hoverPosition.x != clickPosition.x || hoverPosition.y != clickPosition.y) {
      if (selectedIndices.length == 0) {
        for (var i = 0; i < ship.model.points.length; i++) {
          if (ship.model.points[i].x == clickPosition.x && ship.model.points[i].y == clickPosition.y) {
            selectedIndices.push(i);
            break;
          }
        }
      }
      if (selectedIndices.length > 0) {
        isDragging = true;
        for (var i = 0; i < selectedIndices.length; i++) {
          ship.model.points[selectedIndices[i]].x += (hoverPosition.x - clickPosition.x);
          ship.model.points[selectedIndices[i]].y += (hoverPosition.y - clickPosition.y);
        }
        clickPosition.x = hoverPosition.x;
        clickPosition.y = hoverPosition.y;
      }
    }
  };
  
  document.onmouseup = function(event) {
	var canvasBounds = canvas.getBoundingClientRect();
    if (event.clientX < canvasBounds.x || event.clientX > canvas.x + canvasBounds.width || event.clientY < canvasBounds.y || event.clientY > canvas.y + canvasBounds.height)
	  return;
    if (!isDragging) {
      for (var i = 0; i < ship.model.points.length; i++) {
        if (ship.model.points[i].x == clickPosition.x && ship.model.points[i].y == clickPosition.y) {
          if (event.ctrlKey) {
            for (var j = 0; j < selectedIndices.length; j++) {
              if (selectedIndices[j] == i) {
                selectedIndices.splice(j, 1);
                isDragging = false;
                clickPosition.x = -1;
                clickPosition.y = -1;
                return;
              }
            }
          } else {
            while (selectedIndices.length > 0)
              selectedIndices.pop();
          }
          selectedIndices.push(i);
          isDragging = false;
          clickPosition.x = -1;
          clickPosition.y = -1;
          return;
        }
      }
      while (selectedIndices.length > 0)
        selectedIndices.pop();
    }
    isDragging = false;
    clickPosition.x = -1;
    clickPosition.y = -1;
  };

  window.setInterval(function() {
    ctx.setTransform(1, 0, 0, 1, 0, 0);
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    drawGrid(ctx, 320, 180, 32, 32, 10, 10);
    drawShip(ctx, ship.model, canvas.width / 2, canvas.height / 2, scaleFactor);
	drawShip(ctx, ship.model, canvas.width - ship.model.width - 5, canvas.height - ship.model.height - 5, { x: 1, y: 1 });
    drawNodes(ctx, ship.model, canvas.width / 2, canvas.height / 2, scaleFactor);
    drawSelectedNodes(ctx, ship.model, canvas.width / 2, canvas.height / 2, scaleFactor, selectedIndices);
  }, 16);

  function drawGrid(ctx, x, y, width, height, cellWidth, cellHeight) {
    ctx.shadowBlur = 2;
    ctx.shadowColor = 'gray';
    ctx.strokeStyle = 'gray';
    ctx.lineWidth = 1;
    ctx.beginPath();
    for (var row = 0; row <= height; row++) {
      ctx.moveTo(x - (width / 2) * cellWidth + 0.5, y - (height / 2) * cellHeight + row * cellHeight + 0.5);
      ctx.lineTo(x + (width / 2) * cellWidth + 0.5, y - (height / 2) * cellHeight + row * cellHeight + 0.5);
    }
    for (var column = 0; column <= width; column++) {
      ctx.moveTo(x - (width / 2) * cellWidth + column * cellWidth + 0.5, y - (height / 2) * cellHeight + 0.5);
      ctx.lineTo(x - (width / 2) * cellWidth + column * cellWidth + 0.5, y + (height / 2) * cellHeight + 0.5);
    }
    ctx.stroke();
    ctx.shadowColor = 'white';
    ctx.strokeStyle = 'white';
    ctx.shadowBlur = 2;
    ctx.lineWidth = 1;
    ctx.beginPath();
    for (var row = 0; row <= height; row += height / 4) {
      ctx.moveTo(x - (width / 2) * cellWidth + 0.5, y - (height / 2) * cellHeight + row * cellHeight + 0.5);
      ctx.lineTo(x + (width / 2) * cellWidth + 0.5, y - (height / 2) * cellHeight + row * cellHeight + 0.5);
    }
    for (var column = 0; column <= width; column += width / 4) {
      ctx.moveTo(x - (width / 2) * cellWidth + column * cellWidth + 0.5, y - (height / 2) * cellHeight + 0.5);
      ctx.lineTo(x - (width / 2) * cellWidth + column * cellWidth + 0.5, y + (height / 2) * cellHeight + 0.5);
    }
    ctx.stroke();
  }
  
  function drawShip(ctx, model, x, y, scaleFactor) {
    x -= (model.width >> 1) * scaleFactor.x;
    y -= (model.height >> 1) * scaleFactor.y;

    ctx.beginPath();
    ctx.shadowBlur = 3;
    ctx.shadowColor = 'white';
    ctx.fillStyle = 'white';
    ctx.moveTo(x + model.points[0].x * scaleFactor.x, y + model.points[0].y * scaleFactor.y);
    for (var i = 1; i < model.points.length; i++)
      ctx.lineTo(x + model.points[i].x * scaleFactor.x, y + model.points[i].y * scaleFactor.y);
    if (model.mirrorY) {
      for (var i = model.points.length - 1; i >= 0; i--)
        ctx.lineTo(x + model.points[i].x * scaleFactor.x, y + (model.height - model.points[i].y) * scaleFactor.y);
	}
    ctx.closePath();
    ctx.fill();
  }
  
  function drawNodes(ctx, model, x, y, scaleFactor) {
    x -= (model.width >> 1) * scaleFactor.x;
    y -= (model.height >> 1) * scaleFactor.y;

    ctx.shadowBlur = 3;
    ctx.shadowColor = 'cyan';
    ctx.fillStyle = 'cyan';
    for (var i = 0; i < model.points.length; i++) {
      ctx.beginPath();
      ctx.arc(x + model.points[i].x * scaleFactor.x, y + model.points[i].y * scaleFactor.y, 0.5, 0, 2 * Math.PI);
      ctx.fill();
    }
  }
  
  function drawSelectedNodes(ctx, model, x, y, scaleFactor, selectedIndices) {
    x -= (model.width >> 1) * scaleFactor.x;
    y -= (model.height >> 1) * scaleFactor.y;

    ctx.shadowBlur = 3;
    ctx.shadowColor = 'red';
    ctx.fillStyle = 'red';
    for (var i = 0; i < selectedIndices.length; i++) {
      ctx.beginPath();
      ctx.arc(x + model.points[selectedIndices[i]].x * scaleFactor.x, y + model.points[selectedIndices[i]].y * scaleFactor.y, 5, 0, 2 * Math.PI);
      ctx.fill();
    }
  }
}